package com.steve.sta.service;

import com.steve.sta.model.dao.StockDao;
import com.steve.sta.model.dto.StockDto;
import com.steve.sta.model.mapper.Impl.StockModelMapperImpl;
import com.steve.sta.respository.IStockRepository;
import com.steve.sta.service.Impl.StockServiceImpl;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.modelmapper.ModelMapper;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.when;

public class StockServiceTest {
    @InjectMocks
    StockServiceImpl stockService;

    @Mock
    private StockModelMapperImpl stockModelMapper;

    @Mock
    private IStockRepository stockRepository;

    @BeforeEach
    public void init()
    {
        ModelMapper modelMapper = new ModelMapper();
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void getAllStockTest()
    {

        List<StockDao> stockDaoList = new ArrayList<>();
        StockDao stockOne = new StockDao();
        StockDao stockTwo = new StockDao();
        StockDao stockThree = new StockDao();

        stockOne.setId(1L);
        stockOne.setCompanyName("Netflix");
        stockOne.setSymbol("nflx");
        stockOne.setPrice(new BigDecimal(345.900));


        stockTwo.setId(2L);
        stockTwo.setCompanyName("Apple");
        stockTwo.setSymbol("Appl");
        stockTwo.setPrice(new BigDecimal(5.900));

        stockThree.setId(3L);
        stockThree.setCompanyName("Facebook");
        stockThree.setSymbol("fb");
        stockThree.setPrice(new BigDecimal(895.900));

        stockDaoList.add(stockOne);
        stockDaoList.add(stockTwo);
        stockDaoList.add(stockThree);
        System.out.println("stockDaoList:>>> "+stockDaoList);

        List<StockDto> listGroupDto =stockModelMapper.toStockDtoList(stockDaoList);
        System.out.println("listGroupDto:>>> "+listGroupDto);
        when(stockService.findAll()).thenReturn(listGroupDto);

        //test
        List<StockDto> list = stockService.findAll();

      //  assertEquals (3,list.size());
    }

    public void buyStockTest()
    {

    }

    public void sellStockTest()
    {

    }
}

