package com.steve.sta.respository;

import com.steve.sta.model.dao.UserInformationDao;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;


@Repository
public interface IUserInformationRepository extends JpaRepository<UserInformationDao,Long> {
    Boolean existsByUsername(String username);
    UserInformationDao findByUsername(String username);
}
