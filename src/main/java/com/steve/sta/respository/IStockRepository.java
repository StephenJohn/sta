package com.steve.sta.respository;
import com.steve.sta.model.dao.StockDao;
import com.steve.sta.model.dto.StockDto;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface IStockRepository extends JpaRepository<StockDao, Long> {
    List<StockDao> findStockByUser_Id(long userId);
}
