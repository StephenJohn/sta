package com.steve.sta.respository;
import com.steve.sta.model.dao.StockTransactionHistoryDao;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import java.util.List;

@Repository
public interface IStockTransactionHistoryRepository extends JpaRepository<StockTransactionHistoryDao,Long> {
   // List<StockTransactionHistoryDao> findByUserId(long userId);
    List<StockTransactionHistoryDao> findByStockId(long stockId);
}
