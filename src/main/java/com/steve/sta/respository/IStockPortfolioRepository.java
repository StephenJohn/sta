package com.steve.sta.respository;

import com.steve.sta.model.dao.StockPortfolioDao;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface IStockPortfolioRepository extends JpaRepository<StockPortfolioDao, Long> {
    Boolean existsByCompanyName(String username);
    Boolean existsBySymbol(String username);
    StockPortfolioDao findBySymbol(String symbol);
}
