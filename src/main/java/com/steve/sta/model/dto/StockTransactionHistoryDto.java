package com.steve.sta.model.dto;

import com.steve.sta.model.dao.UserInformationDao;
import com.steve.sta.util.enums.StockTransactionType;
import lombok.Data;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

@Data
public class StockTransactionHistoryDto extends BaseEntityDto {

    private long sockQuoteId;
    private long stockId;
    @Enumerated(EnumType.STRING)
    private StockTransactionType stockTransactionType;

    @ManyToOne
    @JoinColumn(name="userId")
    private UserInformationDto userInformation;
}
