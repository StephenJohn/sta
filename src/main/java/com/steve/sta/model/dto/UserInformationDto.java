package com.steve.sta.model.dto;
import com.steve.sta.util.enums.RecordStatus;
import lombok.Builder;
import lombok.Data;

import javax.persistence.*;
import java.util.Date;
import java.util.List;
import java.util.Set;


@Data
public class UserInformationDto extends BaseEntityDto {
    private String username;
    private String password;
    private String fullname;




    public UserInformationDto() {

    }

    public UserInformationDto(String username, String password, String fullname) {
        this.username = username;
        this.password = password;
        this.fullname = fullname;
    }
}
