package com.steve.sta.model.dto;
import com.steve.sta.model.dao.UserInformationDao;
import com.steve.sta.model.dto.UserInformationDto;
import lombok.Data;

import javax.persistence.JoinColumn;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import java.math.BigDecimal;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

@Data
public class StockDto extends BaseEntityDto {
    private long stockQuoteId;
    private BigDecimal price;
    private String companyName;
    private String userName;
    private String symbol;
    private String transactionStatus;

}
