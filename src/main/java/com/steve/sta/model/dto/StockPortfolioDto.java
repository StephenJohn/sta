package com.steve.sta.model.dto;
import lombok.Data;

import javax.persistence.Entity;


@Data
public class StockPortfolioDto extends BaseEntityDto {
    private String companyName;
    private String symbol;
}
