package com.steve.sta.model.dto;
import com.steve.sta.util.enums.RecordStatus;
import lombok.Data;
import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;


@Data
@MappedSuperclass
public class BaseEntityDto implements Serializable
{
    private Long id;
    @Temporal(TemporalType.DATE)
    private Date createOn;
    @Temporal(TemporalType.DATE)
    private Date updatedOn;
}
