package com.steve.sta.model.dto;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import lombok.Data;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;
import java.io.Serializable;
@Data
public class StockQuoteDto extends BaseEntityDto{
    @SerializedName("symbol")
    @Expose
    private String symbol;
    @SerializedName("companyName")
    @Expose
    private String companyName;
    @SerializedName("primaryExchange")
    @Expose
    private String primaryExchange;
    @SerializedName("calculationPrice")
    @Expose
    private String calculationPrice;
    @SerializedName("open")
    @Expose
    private Double open;
    @SerializedName("openTime")
    @Expose
    private String openTime;
    @SerializedName("openSource")
    @Expose
    private String openSource;
    @SerializedName("close")
    @Expose
    private Double close;
    @SerializedName("closeTime")
    @Expose
    private String closeTime;
    @SerializedName("closeSource")
    @Expose
    private String closeSource;
    @SerializedName("high")
    @Expose
    private String high;
    @SerializedName("highTime")
    @Expose
    private String highTime;
    @SerializedName("highSource")
    @Expose
    private String highSource;
    @SerializedName("low")
    @Expose
    private String low;
    @SerializedName("lowTime")
    @Expose
    private String lowTime;
    @SerializedName("lowSource")
    @Expose
    private String lowSource;
    @SerializedName("latestPrice")
    @Expose
    private Double latestPrice;
    @SerializedName("latestSource")
    @Expose
    private String latestSource;
    @SerializedName("latestTime")
    @Expose
    private String latestTime;
    @SerializedName("latestUpdate")
    @Expose
    private String latestUpdate;
    @SerializedName("latestVolume")
    @Expose
    private Integer latestVolume;
    @SerializedName("iexRealtimePrice")
    @Expose
    private Double iexRealtimePrice;
    @SerializedName("iexRealtimeSize")
    @Expose
    private String iexRealtimeSize;
    @SerializedName("iexLastUpdated")
    @Expose
    private String iexLastUpdated;
    @SerializedName("delayedPrice")
    @Expose
    private Double delayedPrice;
    @SerializedName("delayedPriceTime")
    @Expose
    private String delayedPriceTime;
    @SerializedName("oddLotDelayedPrice")
    @Expose
    private Double oddLotDelayedPrice;
    @SerializedName("oddLotDelayedPriceTime")
    @Expose
    private String oddLotDelayedPriceTime;
    @SerializedName("extendedPrice")
    @Expose
    private Double extendedPrice;
    @SerializedName("extendedChange")
    @Expose
    private Double extendedChange;
    @SerializedName("extendedChangePercent")
    @Expose
    private String extendedChangePercent;
    @SerializedName("extendedPriceTime")
    @Expose
    private String extendedPriceTime;
    @SerializedName("previousClose")
    @Expose
    private Double previousClose;
    @SerializedName("previousVolume")
    @Expose
    private Integer previousVolume;
    @SerializedName("change")
    @Expose
    private Double change;
    @SerializedName("changePercent")
    @Expose
    private Double changePercent;
    @SerializedName("volume")
    @Expose
    private Integer volume;
    @SerializedName("iexMarketPercent")
    @Expose
    private Double iexMarketPercent;
    @SerializedName("iexVolume")
    @Expose
    private Integer iexVolume;
    @SerializedName("avgTotalVolume")
    @Expose
    private Integer avgTotalVolume;
    @SerializedName("iexBidPrice")
    @Expose
    private Integer iexBidPrice;
    @SerializedName("iexBidSize")
    @Expose
    private Integer iexBidSize;
    @SerializedName("iexAskPrice")
    @Expose
    private Integer iexAskPrice;
    @SerializedName("iexAskSize")
    @Expose
    private Integer iexAskSize;
    @SerializedName("iexOpen")
    @Expose
    private String iexOpen;
    @SerializedName("iexOpenTime")
    @Expose
    private String iexOpenTime;
    @SerializedName("iexClose")
    @Expose
    private String iexClose;
    @SerializedName("iexCloseTime")
    @Expose
    private String iexCloseTime;
    @SerializedName("marketCap")
    @Expose
    private String marketCap;
    @SerializedName("peRatio")
    @Expose
    private Double peRatio;
    @SerializedName("week52High")
    @Expose
    private Double week52High;
    @SerializedName("week52Low")
    @Expose
    private Double week52Low;
    @SerializedName("ytdChange")
    @Expose
    private String ytdChange;
    @SerializedName("lastTradeTime")
    @Expose
    private String lastTradeTime;
    @SerializedName("isUSMarketOpen")
    @Expose
    private Boolean isUSMarketOpen;

    @Override
    public String toString() {
        return new ToStringBuilder(this).append("symbol", symbol).
                append("companyName", companyName).append("primaryExchange", primaryExchange).
                append("calculationPrice", calculationPrice).append("open", open).append("openTime", openTime).
                append("openSource", openSource).append("close", close).append("closeTime", closeTime).
                append("closeSource", closeSource).append("high", high).append("highTime", highTime).append("highSource",
                highSource).append("low", low).append("lowTime", lowTime).append("lowSource", lowSource).append("latestPrice",
                latestPrice).append("latestSource", latestSource).append("latestTime", latestTime).append("latestUpdate", latestUpdate).
                append("latestVolume", latestVolume).append("iexRealtimePrice", iexRealtimePrice).append("iexRealtimeSize", iexRealtimeSize).
                append("iexLastUpdated", iexLastUpdated).append("delayedPrice", delayedPrice).append("delayedPriceTime", delayedPriceTime).
                append("oddLotDelayedPrice", oddLotDelayedPrice).append("oddLotDelayedPriceTime",
                oddLotDelayedPriceTime).append("extendedPrice", extendedPrice).append("extendedChange", extendedChange).
                append("extendedChangePercent", extendedChangePercent).append("extendedPriceTime", extendedPriceTime).
                append("previousClose", previousClose).append("previousVolume", previousVolume).
                append("change", change).append("changePercent", changePercent).
                append("volume", volume).append("iexMarketPercent", iexMarketPercent).
                append("iexVolume", iexVolume).append("avgTotalVolume", avgTotalVolume).
                append("iexBidPrice", iexBidPrice).append("iexBidSize", iexBidSize).
                append("iexAskPrice", iexAskPrice).append("iexAskSize", iexAskSize).
                append("iexOpen", iexOpen).append("iexOpenTime", iexOpenTime).
                append("iexClose", iexClose).append("iexCloseTime", iexCloseTime).
                append("marketCap", marketCap).append("peRatio", peRatio).
                append("week52High", week52High).append("week52Low", week52Low).append("ytdChange", ytdChange)
                .append("lastTradeTime", lastTradeTime).append("isUSMarketOpen", isUSMarketOpen).toString();
    }

        @Override
        public int hashCode () {
            return new HashCodeBuilder().append(symbol).append(highTime).append(avgTotalVolume).append(companyName).append(openSource).append(delayedPrice).append(iexMarketPercent).append(primaryExchange).append(latestUpdate).append(high).append(iexOpenTime).append(delayedPriceTime).append(extendedPrice).append(week52Low).append(highSource).append(latestPrice).append(marketCap).append(iexClose).append(volume).append(ytdChange).append(lastTradeTime).append(closeSource).append(extendedChange).append(iexRealtimePrice).append(calculationPrice).append(extendedChangePercent).append(latestSource).append(iexOpen).append(iexBidPrice).append(previousClose).append(peRatio).append(isUSMarketOpen).append(low).append(oddLotDelayedPrice).append(extendedPriceTime).append(closeTime).append(changePercent).append(week52High).append(openTime).append(close).append(iexCloseTime).append(oddLotDelayedPriceTime).append(previousVolume).append(iexRealtimeSize).append(iexLastUpdated).append(change).append(latestVolume).append(iexAskPrice).append(lowSource).append(iexVolume).append(iexAskSize).append(latestTime).append(open).append(lowTime).append(iexBidSize).toHashCode();
        }

        @Override
        public boolean equals (Object other){
            if (other == this) {
                return true;
            }
            if ((other instanceof StockQuoteDto) == false) {
                return false;
            }
            StockQuoteDto rhs = ((StockQuoteDto) other);
            return new EqualsBuilder().append(symbol, rhs.symbol).append(highTime, rhs.highTime).append(avgTotalVolume, rhs.avgTotalVolume).append(companyName, rhs.companyName).append(openSource, rhs.openSource).append(delayedPrice, rhs.delayedPrice).append(iexMarketPercent, rhs.iexMarketPercent).append(primaryExchange, rhs.primaryExchange).append(latestUpdate, rhs.latestUpdate).append(high, rhs.high).append(iexOpenTime, rhs.iexOpenTime).append(delayedPriceTime, rhs.delayedPriceTime).append(extendedPrice, rhs.extendedPrice).append(week52Low, rhs.week52Low).append(highSource, rhs.highSource).append(latestPrice, rhs.latestPrice).append(marketCap, rhs.marketCap).append(iexClose, rhs.iexClose).append(volume, rhs.volume).append(ytdChange, rhs.ytdChange).append(lastTradeTime, rhs.lastTradeTime).append(closeSource, rhs.closeSource).append(extendedChange, rhs.extendedChange).append(iexRealtimePrice, rhs.iexRealtimePrice).append(calculationPrice, rhs.calculationPrice).append(extendedChangePercent, rhs.extendedChangePercent).append(latestSource, rhs.latestSource).append(iexOpen, rhs.iexOpen).append(iexBidPrice, rhs.iexBidPrice).append(previousClose, rhs.previousClose).append(peRatio, rhs.peRatio).append(isUSMarketOpen, rhs.isUSMarketOpen).append(low, rhs.low).append(oddLotDelayedPrice, rhs.oddLotDelayedPrice).append(extendedPriceTime, rhs.extendedPriceTime).append(closeTime, rhs.closeTime).append(changePercent, rhs.changePercent).append(week52High, rhs.week52High).append(openTime, rhs.openTime).append(close, rhs.close).append(iexCloseTime, rhs.iexCloseTime).append(oddLotDelayedPriceTime, rhs.oddLotDelayedPriceTime).append(previousVolume, rhs.previousVolume).append(iexRealtimeSize, rhs.iexRealtimeSize).append(iexLastUpdated, rhs.iexLastUpdated).append(change, rhs.change).append(latestVolume, rhs.latestVolume).append(iexAskPrice, rhs.iexAskPrice).append(lowSource, rhs.lowSource).append(iexVolume, rhs.iexVolume).append(iexAskSize, rhs.iexAskSize).append(latestTime, rhs.latestTime).append(open, rhs.open).append(lowTime, rhs.lowTime).append(iexBidSize, rhs.iexBidSize).isEquals();
        }
    }


