package com.steve.sta.model.mapper.Impl;

import com.steve.sta.model.dao.UserInformationDao;
import com.steve.sta.model.dto.UserInformationDto;
import com.steve.sta.model.mapper.IUserInformationModelMapper;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Arrays;
import java.util.List;

@Component
public class UserInformationModelMapperImpl implements IUserInformationModelMapper {

    private final ModelMapper modelMapper;

    @Autowired
    public UserInformationModelMapperImpl(ModelMapper modelMapper)
    {
        this.modelMapper = modelMapper;
    }

    @Override
    public UserInformationDto toUserInformationDto(UserInformationDao userInformationDao) {
        return modelMapper.map(userInformationDao, UserInformationDto.class);
    }


    @Override
    public UserInformationDao toUserInformationDao(UserInformationDto userInformationDto) {
        return modelMapper.map(userInformationDto,UserInformationDao.class);
    }

    @Override
    public List<UserInformationDto> toUserInformationDtoList(List<UserInformationDao> userInformationDaoList) {
        List<UserInformationDto> userInformationDtoList = Arrays.asList(modelMapper.map(userInformationDaoList, UserInformationDto[].class));
        return userInformationDtoList;
    }

    @Override
    public List<UserInformationDao> toUserInformationDaoList(List<UserInformationDto> userInformationDtoList) {
        List<UserInformationDao> userInformationDaoDaoList = Arrays.asList(modelMapper.map(userInformationDtoList, UserInformationDao[].class));
        return userInformationDaoDaoList;
    }
}
