package com.steve.sta.model.mapper;
import com.steve.sta.model.dao.StockTransactionHistoryDao;
import com.steve.sta.model.dto.StockTransactionHistoryDto;
import org.springframework.stereotype.Component;

import java.util.List;
public interface IStockTransactionHistoryModelMapper{ //extends IBaseModelMapper<StockTransactionHistoryDto,StockTransactionHistoryDao>{
    public StockTransactionHistoryDto toStockTransactionHistoryDto(StockTransactionHistoryDao stockTransactionHistoryDao);
    public StockTransactionHistoryDao toStockTransactionHistoryDao(StockTransactionHistoryDto stockTransactionHistoryDto);
    public List<StockTransactionHistoryDto> toStockTransactionHistoryDtoLst(List<StockTransactionHistoryDao> stockTransactionHistoryDaoList);
    public List<StockTransactionHistoryDao> toStockTransactionHistoryDaoLst(List<StockTransactionHistoryDto> stockTransactionHistoryDtoList);
}
