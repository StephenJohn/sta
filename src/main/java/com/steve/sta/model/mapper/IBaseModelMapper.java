package com.steve.sta.model.mapper;

import com.steve.sta.model.dao.BaseEntityDao;
import com.steve.sta.model.dto.BaseEntityDto;

import java.util.List;

public interface IBaseModelMapper<Dto,Dao> {
    public Dto toDto(Dao  t2);
    public Dao toDao(Dto t1);
    public List<Dto> toDtoList (List<Dao> t2);
    public List<Dao> toDaoList(List<Dto> t1);
}
