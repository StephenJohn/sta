package com.steve.sta.model.mapper;
import com.steve.sta.model.dao.StockQuoteDao;
import com.steve.sta.model.dto.StockQuoteDto;
import java.util.List;

public interface IStockQuoteModelMapper{  //extends IBaseModelMapper<StockQuoteDto,StockQuoteDao> {
    public StockQuoteDto toStockQuoteDto(StockQuoteDao stockQuoteDao);
    public StockQuoteDao toStockQuoteDao(StockQuoteDto stockQuoteDto);
    public List<StockQuoteDto> toStockQuoteDto(List<StockQuoteDao> stockQuoteDaoList);
    public List<StockQuoteDao> toStockQuoteDao(List<StockQuoteDto> stockQuoteDtoList);
}
