package com.steve.sta.model.mapper.Impl;

import com.steve.sta.model.dao.StockTransactionHistoryDao;
import com.steve.sta.model.dto.StockPortfolioDto;
import com.steve.sta.model.dto.StockTransactionHistoryDto;
import com.steve.sta.model.mapper.IStockTransactionHistoryModelMapper;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Arrays;
import java.util.List;

@Component
public class StockTransactionHistoryModelMapperImpl implements IStockTransactionHistoryModelMapper {
    private final ModelMapper modelMapper;

    @Autowired
    public StockTransactionHistoryModelMapperImpl(ModelMapper modelMapper) {
        this.modelMapper = modelMapper;
    }

    @Override
    public StockTransactionHistoryDto toStockTransactionHistoryDto(StockTransactionHistoryDao stockTransactionHistoryDao) {
        return modelMapper.map(stockTransactionHistoryDao, StockTransactionHistoryDto.class);
    }


    @Override
    public StockTransactionHistoryDao toStockTransactionHistoryDao(StockTransactionHistoryDto stockTransactionHistoryDto) {
        return null;
    }

    @Override
    public List<StockTransactionHistoryDto> toStockTransactionHistoryDtoLst(List<StockTransactionHistoryDao> stockTransactionHistoryDaoList) {
        List<StockTransactionHistoryDto> stockTransactionHistoryDtoList = Arrays.asList(modelMapper.map(stockTransactionHistoryDaoList, StockTransactionHistoryDto[].class));
        return stockTransactionHistoryDtoList;
    }

    @Override
    public List<StockTransactionHistoryDao> toStockTransactionHistoryDaoLst(List<StockTransactionHistoryDto> stockTransactionHistoryDtoList) {
        List<StockTransactionHistoryDao> stockTransactionHistoryDaoList = Arrays.asList(modelMapper.map(stockTransactionHistoryDtoList, StockTransactionHistoryDao[].class));
        return stockTransactionHistoryDaoList;
    }
}
