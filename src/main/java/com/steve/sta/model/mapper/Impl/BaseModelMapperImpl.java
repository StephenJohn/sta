package com.steve.sta.model.mapper.Impl;

import com.steve.sta.model.dao.BaseEntityDao;
import com.steve.sta.model.dto.BaseEntityDto;
import com.steve.sta.model.mapper.IBaseModelMapper;
import org.modelmapper.ModelMapper;

import java.util.List;

public class BaseModelMapperImpl<Dto extends BaseEntityDto,Dta extends BaseEntityDao,MM extends ModelMapper> implements IBaseModelMapper {

    private final MM modelMapper;


    public BaseModelMapperImpl(MM modelMapper) {
        this.modelMapper = modelMapper;
    }

    @Override
    public Object toDto(Object t2) {
        return null;
    }

    @Override
    public Object toDao(Object t1) {
        return null;
    }

    @Override
    public List toDtoList(List t2) {
        return null;
    }

    @Override
    public List toDaoList(List t1) {
        return null;
    }




   /* @Override
    public T1 toT1(T2 o) {
        return null;
    }

    @Override
    public T2 toT2(Object o) {
        return modelMapper.map(o);
    }

    @Override
    public List<T1> toT1List(List t2) {
        return null;
    }

    @Override
    public List<T2> toT2List(List t1) {
        return null;
    }*/
}
