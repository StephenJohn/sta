package com.steve.sta.model.mapper;

import com.steve.sta.model.dao.UserInformationDao;
import com.steve.sta.model.dto.UserInformationDto;
import java.util.List;



public interface IUserInformationModelMapper {
    public UserInformationDto toUserInformationDto(UserInformationDao userInformationDao);
    public UserInformationDao toUserInformationDao(UserInformationDto userInformationDto);
    public List<UserInformationDto> toUserInformationDtoList(List<UserInformationDao> userInformationDaoList);
    public List<UserInformationDao> toUserInformationDaoList(List<UserInformationDto> userInformationDtoList);
}