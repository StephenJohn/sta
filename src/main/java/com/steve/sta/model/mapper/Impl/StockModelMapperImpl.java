package com.steve.sta.model.mapper.Impl;

import com.steve.sta.model.dao.StockDao;
import com.steve.sta.model.dto.StockDto;
import com.steve.sta.model.mapper.IStockModelMapper;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Component;

import java.util.Arrays;
import java.util.List;
@Component
public class StockModelMapperImpl implements IStockModelMapper {
    private final ModelMapper modelMapper;

    public StockModelMapperImpl(ModelMapper modelMapper) {
        this.modelMapper = modelMapper;
    }

    @Override
    public StockDto toStockDto(StockDao stockDao) {
        return modelMapper.map(stockDao, StockDto.class);
    }

    @Override
    public StockDao toStockDao(StockDto stockDto) {
        return modelMapper.map(stockDto, StockDao.class);
    }

    @Override
    public List<StockDto> toStockDtoList(List<StockDao> stockDaoList) {
        List<StockDto> stockQuoteDtoList = Arrays.asList(modelMapper.map(stockDaoList, StockDto[].class));
        return stockQuoteDtoList;
    }

    @Override
    public List<StockDao> toStockDaoList(List<StockDto> stockDtoList) {
        List<StockDao> stockDaoList = Arrays.asList(modelMapper.map(stockDtoList, StockDao[].class));
        return stockDaoList;
    }
}
