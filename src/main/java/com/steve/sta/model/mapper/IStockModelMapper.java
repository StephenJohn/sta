package com.steve.sta.model.mapper;

import com.steve.sta.model.dao.StockDao;
import com.steve.sta.model.dto.StockDto;

import java.util.List;

public interface IStockModelMapper {
    public StockDto toStockDto(StockDao stockDao);
    public StockDao toStockDao(StockDto stockDto);
    public List<StockDto> toStockDtoList(List<StockDao> stockDaoList);
    public List<StockDao> toStockDaoList(List<StockDto> stockDtoList);
}
