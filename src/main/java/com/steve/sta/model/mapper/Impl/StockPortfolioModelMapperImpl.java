package com.steve.sta.model.mapper.Impl;
import com.steve.sta.model.dao.StockPortfolioDao;
import com.steve.sta.model.dto.StockPortfolioDto;
import com.steve.sta.model.mapper.IStockPortfolioModelMapper;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Arrays;
import java.util.List;

@Component
public class StockPortfolioModelMapperImpl implements IStockPortfolioModelMapper {
    private final ModelMapper modelMapper;

    @Autowired
    public StockPortfolioModelMapperImpl(ModelMapper modelMapper) {
        this.modelMapper = modelMapper;
    }

    @Override
    public StockPortfolioDto toStockPortfolioDto(StockPortfolioDao stockPortfolioDao) {
        return modelMapper.map(stockPortfolioDao, StockPortfolioDto.class);
    }

    @Override
    public StockPortfolioDao toStockPortfolioDao(StockPortfolioDto stockPortfolioDto) {
        return modelMapper.map(stockPortfolioDto, StockPortfolioDao.class);
    }

    @Override
    public List<StockPortfolioDto> toStockPortfolioDtoList(List<StockPortfolioDao> stockPortfolioDaoList) {
        List<StockPortfolioDto> stockQuoteDtoList = Arrays.asList(modelMapper.map(stockPortfolioDaoList, StockPortfolioDto[].class));
        return stockQuoteDtoList;
    }

    @Override
    public List<StockPortfolioDao> toStockPortfolioDaoList(List<StockPortfolioDto> stockPortfolioDtoDtoList) {
        return null;
    }
}
