package com.steve.sta.model.mapper;

import com.steve.sta.model.dao.StockPortfolioDao;
import com.steve.sta.model.dto.StockPortfolioDto;
import java.util.List;

public interface IStockPortfolioModelMapper{ //extends  IBaseModelMapper<StockPortfolioDto,StockPortfolioDao> {
    public StockPortfolioDto toStockPortfolioDto(StockPortfolioDao stockPortfolioDao);
    public StockPortfolioDao toStockPortfolioDao(StockPortfolioDto stockPortfolioDto);
    public List<StockPortfolioDto> toStockPortfolioDtoList(List<StockPortfolioDao> stockPortfolioDaoList);
    public List<StockPortfolioDao> toStockPortfolioDaoList(List<StockPortfolioDto> stockPortfolioDtoDtoList);
}
