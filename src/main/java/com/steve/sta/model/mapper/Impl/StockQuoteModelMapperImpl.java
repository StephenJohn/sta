package com.steve.sta.model.mapper.Impl;

import com.steve.sta.model.dao.StockQuoteDao;
import com.steve.sta.model.dto.StockQuoteDto;
import com.steve.sta.model.mapper.IStockQuoteModelMapper;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Arrays;
import java.util.List;

@Component
public class StockQuoteModelMapperImpl implements IStockQuoteModelMapper {

    private final ModelMapper modelMapper;

    @Autowired
    public StockQuoteModelMapperImpl(ModelMapper modelMapper) {
        this.modelMapper = modelMapper;
    }


    @Override
    public StockQuoteDto toStockQuoteDto(StockQuoteDao stockQuoteDao) {
        return modelMapper.map(stockQuoteDao, StockQuoteDto.class);
    }

    @Override
    public StockQuoteDao toStockQuoteDao(StockQuoteDto stockQuoteDto) {
        return modelMapper.map(stockQuoteDto, StockQuoteDao.class);
    }

    @Override
    public List<StockQuoteDto> toStockQuoteDto(List<StockQuoteDao> stockQuoteDaoList) {
        List<StockQuoteDto> stockQuoteDtoList = Arrays.asList(modelMapper.map(stockQuoteDaoList, StockQuoteDto[].class));
        return stockQuoteDtoList;
    }

    @Override
    public List<StockQuoteDao> toStockQuoteDao(List<StockQuoteDto> stockQuoteDtoList) {
        List<StockQuoteDao> stockQuoteDaoList = Arrays.asList(modelMapper.map(stockQuoteDtoList, StockQuoteDao[].class));
        return stockQuoteDaoList;
    }
}