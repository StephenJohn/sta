package com.steve.sta.model.dao;

import com.steve.sta.util.enums.StockTransactionType;
import lombok.Data;
import javax.persistence.*;


@Data
@Entity
@Table(name="StockTransactionHistory")
public class StockTransactionHistoryDao extends BaseEntityDao {
   private long sockQuoteId;
    @Enumerated(EnumType.STRING)
    private StockTransactionType stockTransactionType;


    @ManyToOne
    @JoinColumn(name="stockId")
    private StockDao stock;
}
