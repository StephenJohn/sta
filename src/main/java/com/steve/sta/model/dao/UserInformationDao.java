package com.steve.sta.model.dao;

import com.steve.sta.util.enums.RecordStatus;
import lombok.Builder;
import lombok.Data;

import javax.persistence.*;
import java.util.Date;
import java.util.List;
import java.util.Set;


@Entity
@Data
@Table(name="User")
public class UserInformationDao extends BaseEntityDao {



    private String username;
    private String password;
    private String fullname;

    public UserInformationDao() {

    }


    public UserInformationDao(String username, String password, String fullname) {
        this.username = username;
        this.password = password;
        this.fullname = fullname;
    }

    @OneToMany(mappedBy = "user")
    private List<StockDao> stocks;

}
