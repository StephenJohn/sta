package com.steve.sta.model.dao;
import lombok.Data;
import javax.persistence.*;

@Data
@Entity
@Table(name="StockPortfolio")
public class StockPortfolioDao extends BaseEntityDao {
    private String companyName;
    private String symbol;
}
