package com.steve.sta.model.dao;
import com.steve.sta.model.dto.BaseEntityDto;
import com.steve.sta.util.enums.RecordStatus;
import lombok.Data;

import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.Date;

@Data
@MappedSuperclass
public class BaseEntityDao implements Serializable
{
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    @Temporal(TemporalType.DATE)
    private Date createOn;
    @Temporal(TemporalType.DATE)
    private Date updatedOn;
    @Enumerated(EnumType.STRING)
    private RecordStatus recordStatus;
}