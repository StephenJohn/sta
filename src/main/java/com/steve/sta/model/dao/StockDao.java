package com.steve.sta.model.dao;
import lombok.Data;
import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Data
@Entity
@Table(name="Stock")
public class StockDao extends BaseEntityDao {
    private long stockQuoteId;
    private BigDecimal price;
    private String companyName;
    private String symbol;
    private String transactionStatus;


    @ManyToOne
    @JoinColumn(name="userId")
    private UserInformationDao user;

    @OneToMany(mappedBy = "stock")
    private List<StockTransactionHistoryDao> stockTransactionHistorys;
}
