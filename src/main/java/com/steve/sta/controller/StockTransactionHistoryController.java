package com.steve.sta.controller;

import com.steve.sta.model.dto.StockTransactionHistoryDto;
import com.steve.sta.service.infrastructure.IStockTransactionHistoryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@CrossOrigin
@RequestMapping("/api/StockTransactionHistory")
public class StockTransactionHistoryController extends BaseController<IStockTransactionHistoryService,StockTransactionHistoryDto, Long> {

    private final IStockTransactionHistoryService stockTransactionHistoryService;

    @Autowired
    public StockTransactionHistoryController(IStockTransactionHistoryService stockTransactionHistoryService) {
        super(stockTransactionHistoryService);
        this.stockTransactionHistoryService = stockTransactionHistoryService;
    }

  /*  @GetMapping("/getByStockId/{stockId}")
    public List<StockTransactionHistoryDto> getStockTransactionHistoryByStockId(@PathVariable long stockId)
    {
        return this.stockTransactionHistoryService.findByUserId(stockId);
    }

    @GetMapping("/getByUserId/{userId}")
    public List<StockTransactionHistoryDto> getStockTransactionHistoryByUserId(@PathVariable long userId)
    {
        return this.stockTransactionHistoryService.findByUserId(userId);
    }*/
}
