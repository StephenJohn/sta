package com.steve.sta.controller;
import com.steve.sta.model.dto.StockQuoteDto;
import com.steve.sta.service.infrastructure.IStockQuoteService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.math.BigDecimal;
import java.math.BigInteger;

@RestController
@RequestMapping("/api/stockquote")
public class StockQuoteController extends BaseController<IStockQuoteService,StockQuoteDto, Long> {

    private final IStockQuoteService stockQuoteService;

    @Autowired
    public StockQuoteController(IStockQuoteService stockQuoteService)
    {
        super(stockQuoteService);
        this.stockQuoteService = stockQuoteService;
    }

    @GetMapping("/symbol/{symbol}")
    public StockQuoteDto getStockQuoteBySymbol(@PathVariable String symbol)
    {
        return this.stockQuoteService.getStockQuote(symbol);
    }

    @GetMapping("/currentprice/{symbol}")
    public BigDecimal getStockCurrentPrice(@PathVariable String symbol)
    {
        return this.stockQuoteService.getStockCurrentPrice(symbol);
    }

}
