package com.steve.sta.controller;

import com.steve.sta.service.infrastructure.IService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.List;

public  class BaseController<I extends IService,D,Id> {
    private final I service;

    public BaseController(I service) {
        this.service = service;
    }

    @GetMapping("/get/{id}")
    public D getById(@PathVariable Id id)
    {
        return (D)this.service.findById(id);
    }


    @PostMapping("/create")
    @ResponseStatus(HttpStatus.CREATED)
    public D create(@RequestBody D dto)
    {
        return (D)this.service.save(dto);
    }

    @GetMapping("/all")
    public List<D> getAll()
    {
        return (List<D>) this.service.findAll();
    }

    @PutMapping("/update")
    @ResponseStatus(HttpStatus.OK)
    public D update(@RequestBody D dto)
    {
        return (D) this.service.update(dto);
    }

   /* @DeleteMapping("/delete/{id}")
    public void deleteStockPortfolio(@PathVariable long groupId)
    {
        return this.I.(stockTransactionHistoryDto);
    }*/
}
