package com.steve.sta.controller;

import com.steve.sta.model.dto.StockPortfolioDto;
import com.steve.sta.service.infrastructure.IStockPortfolioService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.List;


@RestController
@CrossOrigin
@RequestMapping("/api/stockportfolio")
public class StockPortfolioController extends BaseController<IStockPortfolioService,StockPortfolioDto, Long> {
    private final IStockPortfolioService stockPortfolioService;

    @Autowired
    public StockPortfolioController(IStockPortfolioService stockPortfolioService) {
        super(stockPortfolioService);
        this.stockPortfolioService = stockPortfolioService;
    }

    @GetMapping("/symbol/{symbol}")
    public StockPortfolioDto getStockPortfolioBySymbol(@PathVariable String symbol)
    {
        return this.stockPortfolioService.findBySymbol(symbol);
    }
}
