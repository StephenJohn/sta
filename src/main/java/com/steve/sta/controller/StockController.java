package com.steve.sta.controller;

import com.steve.sta.model.dto.StockDto;
import com.steve.sta.service.infrastructure.IStockService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@CrossOrigin
@RequestMapping("api/stock")
public class StockController extends BaseController<IStockService, StockDto, Long>  {
   private final IStockService stockService;

    @Autowired
    public StockController(IStockService stockService) {
        super(stockService);
        this.stockService = stockService;
    }


    @PostMapping("/buy")
    @ResponseStatus(HttpStatus.CREATED)
    public StockDto buyStock(@RequestBody StockDto dto)
    {
       return stockService.buyStock(dto);
    }

    @PostMapping("/sell")
    @ResponseStatus(HttpStatus.CREATED)
    public StockDto sellStock(@RequestBody StockDto dto)
    {
     return stockService.sellStock(dto);
    }

 @GetMapping("/CurrentStocks/{userId}")
 @ResponseStatus(HttpStatus.OK)
 public List<StockDto> getAllCurrentStockByUserId(@RequestBody long userId)
 {
  return stockService.getStockByUserId(userId);
 }

}
