package com.steve.sta.controller;
import com.steve.sta.exceptions.Impl.NotFoundException;
import com.steve.sta.model.dto.UserInformationDto;
import com.steve.sta.model.security.LoginRequestDto;
import com.steve.sta.model.security.LoginResponseDto;
import com.steve.sta.security.JwtToken;
import com.steve.sta.service.JwtUserDetailsServiceImpl;
import com.steve.sta.service.infrastructure.IUserInformationService;
import com.steve.sta.util.api.ResponseModel;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.DisabledException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.web.bind.annotation.*;

import javax.validation.ValidationException;
import java.security.NoSuchAlgorithmException;

@RestController
@CrossOrigin
@RequestMapping("/account")
public class AccountController extends BaseController<IUserInformationService,UserInformationDto, Long> {

    private final IUserInformationService userInformationService;

    @Autowired
    public AccountController(IUserInformationService userInformationService) {
        super(userInformationService);
        this.userInformationService = userInformationService;
    }

    @PostMapping("/register")
    public ResponseModel<UserInformationDto> createUser(@RequestBody UserInformationDto userInformationDto) throws NotFoundException {
        return userInformationService.createNewUser(userInformationDto);
    }

    @Autowired
    private AuthenticationManager authenticationManager;

    @Autowired
    private JwtToken jwtToken;

    @Autowired
    private JwtUserDetailsServiceImpl jwtUserDetailsService;


    @RequestMapping(value = "/login", method = RequestMethod.POST)
    public ResponseEntity<?> createAuthenticationToken(@RequestBody LoginRequestDto authenticationRequest) throws Exception {
        authenticate(authenticationRequest.getUsername(), authenticationRequest.getPassword());

        final UserDetails userDetails = jwtUserDetailsService
                .loadUserByUsername(authenticationRequest.getUsername());

        final String token = jwtToken.generateToken(userDetails);
        return ResponseEntity.ok(new LoginResponseDto(token));
    }

    private void authenticate(String username, String password) throws Exception {
        try {
            authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(username, password));
        } catch (DisabledException e) {
            throw new Exception("USER_DISABLED", e);
        } catch (BadCredentialsException e) {
            throw new Exception("INVALID_CREDENTIALS", e);
        }
        catch (Exception e) {
            throw new Exception(e.getStackTrace().toString(), e);
        }
    }
}
