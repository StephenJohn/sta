package com.steve.sta.config;
import com.steve.sta.exceptions.Impl.BadRequestException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;


@ControllerAdvice(basePackages = {"com.steve.sta.controller"})
public class STAAdviceConfig extends ResponseEntityExceptionHandler {
    private static String error="==>>>";
    private static Logger logger = LoggerFactory.getLogger(STAAdviceConfig.class);
    @ExceptionHandler(BadRequestException.class)
    public ResponseEntity<?> handleExceptionAdvice(BadRequestException e) {
        logger.error(error,e);
        return new ResponseEntity<>(e.getStackTrace(), HttpStatus.BAD_REQUEST);
    }
}
