package com.steve.sta.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.ParameterBuilder;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.schema.ModelRef;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.ApiKey;
import springfox.documentation.service.Contact;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

import java.util.function.Predicate;
import static java.util.Collections.singletonList;


@Configuration
@EnableSwagger2
public class SwaggerConfig {
   // @Value("jwt.swaggerToken")
  // private String swaggerToken;


    @Bean
    public Docket api() {
        return new Docket(DocumentationType.SWAGGER_2)
                    .select()
                    .paths(PathSelectors.any())
                    //.paths(PathSelectors.ant("/api/**"))
                    .apis(RequestHandlerSelectors.basePackage("com.steve.sta.controller"))
                    .build()
                    .apiInfo(apiEndPointsInfo())
                .globalOperationParameters(singletonList(
                new ParameterBuilder()
                        .name("Authorization")
                        .modelRef(new ModelRef("string"))
                        .parameterType("header")
                       // .required(true)
                        //.hidden(true)
                       // .defaultValue("Bearer " + swaggerToken)
                .build()
        ));
        }


    private ApiInfo apiEndPointsInfo() {
        return new ApiInfoBuilder().title("Stock Transaction Application REST API")
                .description("Stock Transaction Application REST API")
                .contact(new Contact("Stephen John", "", "stephenjohn.tech@gmail.com"))
                .license("STA 1.0")
                .licenseUrl("/")
                .version("1.0.0")
                .build();
    }

    private ApiKey apiKey() {
        return new ApiKey("jwtToken", "Authorization", "header");
    }

    }
