package com.steve.sta.exceptions;

import org.springframework.http.HttpStatus;

public abstract class STAException extends Exception {
    private HttpStatus code;

    public STAException(HttpStatus code, String message){
        super(message);
        this.code = code;
    }

    public HttpStatus getCode(){
        return this.code;
    }
    public void setCode(HttpStatus code){
        this.code = code;
    }
}