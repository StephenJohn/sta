package com.steve.sta.exceptions.Impl;

import com.steve.sta.exceptions.STAException;
import org.springframework.http.HttpStatus;

import java.util.logging.Logger;

public class NotFoundException extends STAException {
    public NotFoundException(String code, String message) {
        super(HttpStatus.NOT_FOUND,message);
    }
}

