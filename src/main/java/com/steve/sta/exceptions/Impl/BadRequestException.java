package com.steve.sta.exceptions.Impl;
import com.steve.sta.exceptions.STAException;
import org.springframework.http.HttpStatus;

public class BadRequestException extends STAException {
    public BadRequestException(String code, String message) {
        super(HttpStatus.BAD_REQUEST, HttpStatus.BAD_REQUEST.getReasonPhrase());
    }
}
