package com.steve.sta.exceptions.Impl;

import com.steve.sta.exceptions.STAException;
import org.springframework.http.HttpStatus;

public class InternalServerErrorException extends STAException {
    public InternalServerErrorException(String code, String message) {
        super(HttpStatus.INTERNAL_SERVER_ERROR, HttpStatus.INTERNAL_SERVER_ERROR.getReasonPhrase());
    }
}