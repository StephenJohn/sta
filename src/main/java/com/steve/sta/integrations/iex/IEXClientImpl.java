package com.steve.sta.integrations.iex;


import com.steve.sta.model.dao.StockQuoteDao;
import com.steve.sta.model.dto.StockQuoteDto;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

@Component
public class IEXClientImpl implements IIEXClient {

    @Value("${iex.token}")
    private String token;

    @Value("${iex.baseUrl}")
    private String baseUrl;

    private  RestTemplate restTemplate;

    @Bean
    public RestTemplate restTemplate(RestTemplateBuilder builder) {
        restTemplate = builder.build();
        return restTemplate;
    }

    public StockQuoteDto getStockQuote(String symbol)
    {
        String url = baseUrl+"/"+symbol+"/quote?token="+token;
        StockQuoteDto stockQuoteDto = restTemplate.getForObject(url, StockQuoteDto.class);
        return stockQuoteDto;
    }

}
