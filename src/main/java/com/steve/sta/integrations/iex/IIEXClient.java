package com.steve.sta.integrations.iex;

import com.steve.sta.model.dao.StockQuoteDao;
import com.steve.sta.model.dto.StockQuoteDto;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.web.client.RestTemplate;

public interface IIEXClient {
    RestTemplate restTemplate = null;
    public RestTemplate restTemplate(RestTemplateBuilder builder);
    public StockQuoteDto getStockQuote(String symbol);
}
