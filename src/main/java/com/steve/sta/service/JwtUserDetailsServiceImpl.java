package com.steve.sta.service;

import com.steve.sta.model.dao.UserInformationDao;
import com.steve.sta.model.dto.UserInformationDto;
import com.steve.sta.model.mapper.IUserInformationModelMapper;
import com.steve.sta.service.infrastructure.IUserInformationService;
import com.steve.sta.util.api.ResponseModel;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import javax.validation.ValidationException;
import java.util.ArrayList;

@Service
public class JwtUserDetailsServiceImpl implements UserDetailsService {
    @Autowired
    private IUserInformationService userInformationService;

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
       ResponseModel<UserInformationDto>  user = userInformationService.findByUsername(username);
        if (user == null) {
            throw new UsernameNotFoundException("User not found with username: " + username);
        }
        return new org.springframework.security.core.userdetails.User(user.getResponseData().getUsername(), user.getResponseData().getPassword(),new ArrayList<>());
    }
}