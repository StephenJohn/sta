package com.steve.sta.service.infrastructure;
import com.steve.sta.model.dto.StockQuoteDto;

import java.math.BigDecimal;

public interface IStockQuoteService extends IService<StockQuoteDto,Long>{
    public StockQuoteDto getStockQuote(String symbol);
    public BigDecimal getStockCurrentPrice(String symbol);
}
