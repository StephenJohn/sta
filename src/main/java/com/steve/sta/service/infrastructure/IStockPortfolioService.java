package com.steve.sta.service.infrastructure;

import com.steve.sta.model.dao.StockPortfolioDao;
import com.steve.sta.model.dto.StockPortfolioDto;
import java.util.List;

public interface IStockPortfolioService extends IService<StockPortfolioDto,Long>{
    Boolean existsByCompanyName(String companyName);
    Boolean existsBySymbol(String symbol);
    StockPortfolioDto findBySymbol(String username);
}
