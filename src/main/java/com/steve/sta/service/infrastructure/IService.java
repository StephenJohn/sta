package com.steve.sta.service.infrastructure;

import java.util.List;

public interface IService<E,id> {
    List<E> findAll();
    E findById(id id);
    E save(E dto);
    E update(E dto);
}
