package com.steve.sta.service.infrastructure;

import com.steve.sta.model.dao.StockDao;
import com.steve.sta.model.dto.StockTransactionHistoryDto;

import java.util.List;

public interface IStockTransactionHistoryService extends IService<StockTransactionHistoryDto,Long> {
  //  List<StockTransactionHistoryDto>  findByUserId(long userId);
    List<StockTransactionHistoryDto>  findByStockId(long stockId);
    void saveTransactionHistory (StockDao stock);
}
