package com.steve.sta.service.infrastructure;

import com.steve.sta.exceptions.Impl.NotFoundException;
import com.steve.sta.model.dto.UserInformationDto;
import com.steve.sta.util.api.ResponseModel;

public interface IUserInformationService extends IService<UserInformationDto,Long>{
    Boolean existsByUsername(String username);
    ResponseModel<UserInformationDto> findByUsername(String username);
    ResponseModel<UserInformationDto> createNewUser(UserInformationDto userInformationDao) throws NotFoundException;
}

