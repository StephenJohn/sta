package com.steve.sta.service.infrastructure;

import com.steve.sta.model.dao.StockTransactionHistoryDao;
import com.steve.sta.model.dto.StockDto;

import java.util.List;

public interface IStockService extends IService<StockDto,Long>{
      StockDto sellStock(StockDto stockDto);
      StockDto buyStock(StockDto stockDto);
      List<StockDto> getStockByUserId(long userId);
}
