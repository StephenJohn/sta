package com.steve.sta.service.Impl;

import com.steve.sta.exceptions.Impl.NotFoundException;
import com.steve.sta.model.dao.UserInformationDao;
import com.steve.sta.model.dto.UserInformationDto;
import com.steve.sta.model.mapper.IUserInformationModelMapper;
import com.steve.sta.respository.IUserInformationRepository;
import com.steve.sta.service.infrastructure.IUserInformationService;
import com.steve.sta.util.api.IWebApiResponse;
import com.steve.sta.util.api.ResponseModel;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import javax.validation.ValidationException;
import java.util.List;
import java.util.Optional;

@Service
public class UserInformationServiceImpl implements IUserInformationService {
    @Autowired
    private PasswordEncoder passwordEncoder;

    private final IUserInformationRepository userInformationRepository;
    private final IUserInformationModelMapper userInformationModelMapper;
    private final IWebApiResponse<UserInformationDto> webApiResponse;


    @Autowired
    public UserInformationServiceImpl(IUserInformationRepository userInformationRepository,
                                      IUserInformationModelMapper userInformationModelMapper,
                                      IWebApiResponse<UserInformationDto> webApiResponse)
    {
        this.userInformationRepository = userInformationRepository;
        this.userInformationModelMapper =userInformationModelMapper;
        this.webApiResponse = webApiResponse;
    }

    @Override
    public List<UserInformationDto> findAll() {

        List<UserInformationDao> userInformationDaoList = userInformationRepository.findAll();
        return userInformationModelMapper.toUserInformationDtoList(userInformationDaoList);
    }

    @Override
    public UserInformationDto findById(Long id) {
        Optional<UserInformationDao> userInformationDao = userInformationRepository.findById(id);
        return userInformationModelMapper.toUserInformationDto(userInformationDao.get());
    }

    @Override
    public UserInformationDto save(UserInformationDto userInformationDto) {
        UserInformationDao userInformationDao = userInformationModelMapper.toUserInformationDao(userInformationDto);
        userInformationDao = userInformationRepository.save(userInformationDao);
        return userInformationModelMapper.toUserInformationDto(userInformationDao);
    }


    @Override
    public UserInformationDto update(UserInformationDto userInformationDto) {
        return null;
    }


    @Override
    public Boolean existsByUsername(String username) {
        return userInformationRepository.existsByUsername(username);
    }

    @Override
    public ResponseModel<UserInformationDto>  findByUsername(String username) {
       UserInformationDao dao = userInformationRepository.findByUsername(username);
       return this.webApiResponse.successful(userInformationModelMapper.toUserInformationDto(dao), HttpStatus.OK);
    }


    public ResponseModel<UserInformationDto> save(UserInformationDao userInformation) {
        UserInformationDao dao = userInformationRepository.save(userInformation);
        return this.webApiResponse.successful(userInformationModelMapper.toUserInformationDto(dao), HttpStatus.OK);
    }

    public ResponseModel<UserInformationDto> createNewUser(UserInformationDto userInformationDto) throws NotFoundException {
        String username = userInformationDto.getUsername();
        String password = userInformationDto.getPassword();
        if (existsByUsername(username)){
            throw new NotFoundException(HttpStatus.NOT_FOUND.toString(), "Username already existed");
        }

        userInformationDto.setPassword(passwordEncoder.encode(password));
        UserInformationDao dao = userInformationModelMapper.toUserInformationDao(userInformationDto);
        save(dao);
        userInformationDto.setPassword(password);


        return this.webApiResponse.successful(userInformationDto, HttpStatus.OK);
    }
}
