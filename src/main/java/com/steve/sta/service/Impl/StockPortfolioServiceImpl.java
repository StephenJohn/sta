package com.steve.sta.service.Impl;

import com.steve.sta.model.dao.StockPortfolioDao;
import com.steve.sta.model.dto.StockPortfolioDto;
import com.steve.sta.model.mapper.IStockPortfolioModelMapper;
import com.steve.sta.respository.IStockPortfolioRepository;
import com.steve.sta.service.infrastructure.IStockPortfolioService;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class StockPortfolioServiceImpl  implements IStockPortfolioService {
    private final IStockPortfolioRepository stockPortfolioRepository;
    private final IStockPortfolioModelMapper stockPortfolioModelMapper;
    public StockPortfolioServiceImpl(IStockPortfolioRepository stockPortfolioRepository, IStockPortfolioModelMapper stockPortfolioModelMapper) {
        this.stockPortfolioRepository = stockPortfolioRepository;
        this.stockPortfolioModelMapper = stockPortfolioModelMapper;
    }

    @Override
    public Boolean existsByCompanyName(String companyName) {
        return this.stockPortfolioRepository.existsByCompanyName(companyName);
    }

    @Override
    public Boolean existsBySymbol(String symbol) {
        return this.stockPortfolioRepository.existsBySymbol(symbol);
    }

    @Override
    public StockPortfolioDto findBySymbol(String symbol) {
        StockPortfolioDao stockPortfolioDao = this.stockPortfolioRepository.findBySymbol(symbol);
        StockPortfolioDto stockPortfolioDto = this.stockPortfolioModelMapper.toStockPortfolioDto(stockPortfolioDao);
        return stockPortfolioDto;
    }

    @Override
    public StockPortfolioDto save(StockPortfolioDto dto) {
        StockPortfolioDao stockPortfolioDao = this.stockPortfolioModelMapper.toStockPortfolioDao(dto);
        StockPortfolioDao dao = this.stockPortfolioRepository.save(stockPortfolioDao);
        dto = this.stockPortfolioModelMapper.toStockPortfolioDto(dao);
        return dto;
    }

    @Override
    public List<StockPortfolioDto> findAll() {
        List<StockPortfolioDao> stockPortfolioDaoList = stockPortfolioRepository.findAll();
        return stockPortfolioModelMapper.toStockPortfolioDtoList(stockPortfolioDaoList);
    }


    @Override
    public StockPortfolioDto findById(Long id) {
        StockPortfolioDao stockPortfolioDao = this.stockPortfolioRepository.findById(id).get();
        StockPortfolioDto stockPortfolioDto = this.stockPortfolioModelMapper.toStockPortfolioDto(stockPortfolioDao);
        return stockPortfolioDto;
    }



    @Override
    public StockPortfolioDto update(StockPortfolioDto dto) {
        StockPortfolioDao stockPortfolioDao = this.stockPortfolioModelMapper.toStockPortfolioDao(dto);
        StockPortfolioDao dao = this.stockPortfolioRepository.save(stockPortfolioDao);
        return dto;
    }
}
