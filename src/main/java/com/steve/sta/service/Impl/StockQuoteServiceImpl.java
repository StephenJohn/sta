package com.steve.sta.service.Impl;

import com.steve.sta.integrations.iex.IIEXClient;
import com.steve.sta.model.dao.StockQuoteDao;
import com.steve.sta.model.dto.StockQuoteDto;
import com.steve.sta.model.mapper.IStockQuoteModelMapper;
import com.steve.sta.service.infrastructure.IStockQuoteService;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.List;

@Service
public class StockQuoteServiceImpl implements IStockQuoteService {
    private final IIEXClient iexClient;
    private final IStockQuoteModelMapper stockQuoteModelMapperImpl;

    public StockQuoteServiceImpl(IIEXClient iexClient,IStockQuoteModelMapper stockQuoteModelMapperImpl) {
        this.iexClient =iexClient;
        this.stockQuoteModelMapperImpl=stockQuoteModelMapperImpl;
    }

    @Override
    public StockQuoteDto getStockQuote(String symbol) {
        StockQuoteDto stockQuoteDto = this.iexClient.getStockQuote(symbol);
        return stockQuoteDto;
    }

    @Override
    public BigDecimal getStockCurrentPrice(String symbol) {
        StockQuoteDto stockQuoteDto = this.iexClient.getStockQuote(symbol);
        BigDecimal price = new BigDecimal(stockQuoteDto.getClose());
        return price;
    }

    @Override
    public List<StockQuoteDto> findAll() {
        return null;
    }

    @Override
    public StockQuoteDto findById(Long aLong) {
        return null;
    }

    @Override
    public StockQuoteDto save(StockQuoteDto dto) {
        return null;
    }

    @Override
    public StockQuoteDto update(StockQuoteDto dto) {
        return null;
    }
}
