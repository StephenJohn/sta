package com.steve.sta.service.Impl;

import com.steve.sta.model.dao.StockDao;
import com.steve.sta.model.dao.UserInformationDao;
import com.steve.sta.model.dto.StockDto;
import com.steve.sta.model.dto.UserInformationDto;
import com.steve.sta.model.mapper.IStockModelMapper;
import com.steve.sta.model.mapper.IUserInformationModelMapper;
import com.steve.sta.respository.IStockRepository;
import com.steve.sta.service.infrastructure.IStockService;
import com.steve.sta.service.infrastructure.IStockTransactionHistoryService;
import com.steve.sta.service.infrastructure.IUserInformationService;
import com.steve.sta.util.api.ResponseModel;
import com.steve.sta.util.enums.StockTransactionType;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;
@Service
public class StockServiceImpl implements IStockService {

    private final IStockRepository stockRepository;
    private final IStockTransactionHistoryService stockTransactionHistoryService;
    private final IStockModelMapper stockModelMapper;
    private final IUserInformationService userInformationService;
    private final IUserInformationModelMapper userInformationModelMapper;
    @Autowired
    public StockServiceImpl(IStockRepository stockRepository,
                            IStockTransactionHistoryService stockTransactionHistoryService, IStockModelMapper stockModelMapper, IUserInformationService userInformationService, IUserInformationModelMapper userInformationModelMapper) {
        this.stockRepository = stockRepository;
        this.stockTransactionHistoryService = stockTransactionHistoryService;
        this.stockModelMapper = stockModelMapper;
        this.userInformationService = userInformationService;
        this.userInformationModelMapper = userInformationModelMapper;
    }


    @Override
    public List<StockDto> findAll() {
        List<StockDao> stockPortfolioDaoList = stockRepository.findAll();
        return stockModelMapper.toStockDtoList(stockPortfolioDaoList);
    }

    @Override
    public StockDto findById(Long id) {
        StockDao stockDao = this.stockRepository.findById(id).get();
        StockDto stockDto = this.stockModelMapper.toStockDto(stockDao);
        return stockDto;
    }

    @Override
    public StockDto save(StockDto dto) {
        StockDao stockDao = this.stockModelMapper.toStockDao(dto);

        stockDao.setCreateOn(new Date());
        stockDao.setTransactionStatus(StockTransactionType.BUY.toString());
        ResponseModel<UserInformationDto> user = this.userInformationService.findByUsername(dto.getUserName());
        UserInformationDao userDao = this.userInformationModelMapper.toUserInformationDao(user.getResponseData());
        stockDao.setUser(userDao);

        stockDao = this.stockRepository.save(stockDao);
        dto = this.stockModelMapper.toStockDto(stockDao);
        return dto;
    }

    @Override
    public StockDto update(StockDto dto) {
        StockDao stockDao = this.stockModelMapper.toStockDao(dto);
        stockDao = this.stockRepository.save(stockDao);
        dto = this.stockModelMapper.toStockDto(stockDao);
        return dto;
    }


    @Override
    public StockDto buyStock(StockDto stockDto) {
        //Save Stock
        stockDto = this.save(stockDto);
        StockDao stockDao= this.stockModelMapper.toStockDao(stockDto);

        //Save History
        stockTransactionHistoryService.saveTransactionHistory(stockDao);
        return this.stockModelMapper.toStockDto(stockDao);
    }

    @Override
    public StockDto sellStock(StockDto stockDto) {
        //Save Stock
        stockDto.setTransactionStatus(StockTransactionType.SOLD.toString());
        stockDto = this.save(stockDto);
        StockDao stockDao= this.stockModelMapper.toStockDao(stockDto);

        //Save History
        stockTransactionHistoryService.saveTransactionHistory(stockDao);
        return this.stockModelMapper.toStockDto(stockDao);
    }

    @Override
    public List<StockDto> getStockByUserId(long userId) {
        List<StockDao> lstStockDao = this.stockRepository.findStockByUser_Id(userId);
        List<StockDto> lstStockDto = stockModelMapper.toStockDtoList(lstStockDao);
        return lstStockDto;
    }
}
