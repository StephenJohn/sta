package com.steve.sta.service.Impl;

import com.steve.sta.model.dao.StockDao;
import com.steve.sta.model.dao.StockTransactionHistoryDao;
import com.steve.sta.model.dto.StockTransactionHistoryDto;
import com.steve.sta.model.mapper.IStockPortfolioModelMapper;
import com.steve.sta.model.mapper.IStockTransactionHistoryModelMapper;
import com.steve.sta.respository.IStockTransactionHistoryRepository;
import com.steve.sta.service.infrastructure.IStockTransactionHistoryService;
import com.steve.sta.util.enums.StockTransactionType;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;

@Service
public class StockTransactionHistoryServiceImpl implements IStockTransactionHistoryService {
    private final IStockTransactionHistoryRepository stockTransactionHistoryRepository;
    private final IStockTransactionHistoryModelMapper stockTransactionHistoryModelMapper;

    public StockTransactionHistoryServiceImpl(IStockTransactionHistoryRepository stockPortfolioRepository1, IStockTransactionHistoryModelMapper stockPortfolioModelMapper1) {
        this.stockTransactionHistoryRepository = stockPortfolioRepository1;
        this.stockTransactionHistoryModelMapper = stockPortfolioModelMapper1;
    }

   /* @Override
    public List<StockTransactionHistoryDto> findByUserId(long userId) {
        List<StockTransactionHistoryDao> stockTransactionHistoryDaoList = stockTransactionHistoryRepository.findByUserId(userId);
        return stockTransactionHistoryModelMapper.toStockTransactionHistoryDtoLst(stockTransactionHistoryDaoList);
    }*/

    @Override
    public List<StockTransactionHistoryDto> findByStockId(long stockId) {
        List<StockTransactionHistoryDao> stockTransactionHistoryDao = this.stockTransactionHistoryRepository.findByStockId(stockId);
        List<StockTransactionHistoryDto> stockPortfolioDto = this.stockTransactionHistoryModelMapper.toStockTransactionHistoryDtoLst(stockTransactionHistoryDao);
        return stockPortfolioDto;
    }

    @Override
    public void saveTransactionHistory(StockDao stock) {
        StockTransactionHistoryDao stockTransactionHistoryDao = new StockTransactionHistoryDao();
        stockTransactionHistoryDao.setSockQuoteId(stock.getStockQuoteId());
        stockTransactionHistoryDao.setStockTransactionType(StockTransactionType.BUY);
        stockTransactionHistoryDao.setCreateOn(new Date());
        stockTransactionHistoryDao.setUpdatedOn(new Date());
        stockTransactionHistoryRepository.save(stockTransactionHistoryDao);
    }

    @Override
    public StockTransactionHistoryDto save(StockTransactionHistoryDto stockTransactionHistoryDto) {
        StockTransactionHistoryDao stockTransactionHistoryDao = this.stockTransactionHistoryModelMapper.toStockTransactionHistoryDao(stockTransactionHistoryDto);
        stockTransactionHistoryDao = this.stockTransactionHistoryRepository.save(stockTransactionHistoryDao);
        stockTransactionHistoryDto = this.stockTransactionHistoryModelMapper.toStockTransactionHistoryDto(stockTransactionHistoryDao);
        return stockTransactionHistoryDto;
    }

    @Override
    public List<StockTransactionHistoryDto> findAll() {
        List<StockTransactionHistoryDao> userInformationDaoList = stockTransactionHistoryRepository.findAll();
        return stockTransactionHistoryModelMapper.toStockTransactionHistoryDtoLst(userInformationDaoList);
    }



    @Override
    public StockTransactionHistoryDto findById(Long id) {
        StockTransactionHistoryDao stockTransactionHistoryDao = this.stockTransactionHistoryRepository.findById(id).get();
        StockTransactionHistoryDto stockPortfolioDto = this.stockTransactionHistoryModelMapper.toStockTransactionHistoryDto(stockTransactionHistoryDao);
        return stockPortfolioDto;
    }

    @Override
    public StockTransactionHistoryDto update(StockTransactionHistoryDto stockTransactionHistoryDto) {
        StockTransactionHistoryDao stockTransactionHistoryDao = this.stockTransactionHistoryModelMapper.toStockTransactionHistoryDao(stockTransactionHistoryDto);
        stockTransactionHistoryDao = this.stockTransactionHistoryRepository.save(stockTransactionHistoryDao);
        stockTransactionHistoryDto = this.stockTransactionHistoryModelMapper.toStockTransactionHistoryDto(stockTransactionHistoryDao);
        return stockTransactionHistoryDto;
    }
}

