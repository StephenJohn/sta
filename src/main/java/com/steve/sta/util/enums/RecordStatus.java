package com.steve.sta.util.enums;

public enum RecordStatus {
     ACTIVE,
    INACTIVE,
    DELETED,
}
