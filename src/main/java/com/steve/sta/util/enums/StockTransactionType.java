package com.steve.sta.util.enums;

public enum StockTransactionType {
    BUY,
    SOLD
}
