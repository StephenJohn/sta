package com.steve.sta.util.api;
import lombok.Data;
import org.springframework.http.HttpStatus;

@Data
public class ResponseModel<T> {
    private String message;
    private HttpStatus responseCode;
    private T responseData;
    private boolean requestSuccessful;
}
