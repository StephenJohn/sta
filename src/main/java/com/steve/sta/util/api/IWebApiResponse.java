package com.steve.sta.util.api;

import org.springframework.http.HttpStatus;

public interface IWebApiResponse<T> {
    public ResponseModel errorOccured(String errorMsg, HttpStatus status);
    public ResponseModel<T> successful(T model, HttpStatus status);
}
