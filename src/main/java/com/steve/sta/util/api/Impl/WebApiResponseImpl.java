package com.steve.sta.util.api.Impl;
import com.steve.sta.util.api.IWebApiResponse;
import com.steve.sta.util.api.ResponseModel;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;

@Component
public class WebApiResponseImpl<T> implements IWebApiResponse {
    @Override
    public ResponseModel errorOccured(String errorMsg, HttpStatus status) {
        ResponseModel responseModel = new ResponseModel();
        responseModel.setRequestSuccessful(false);
        responseModel.setMessage(errorMsg);
        responseModel.setResponseCode(status);
        responseModel.setResponseData(null);
        return responseModel;
    }

    @Override
    public ResponseModel successful(Object model, HttpStatus status) {
        ResponseModel responseModel = new ResponseModel();
        responseModel.setRequestSuccessful(true);
        responseModel.setMessage("Successfull");
        responseModel.setResponseCode(status);
        responseModel.setResponseData((T)model);
        return responseModel;
    }
}